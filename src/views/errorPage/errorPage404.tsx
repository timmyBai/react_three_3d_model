import React from 'react';

// css
import './errorPage404.sass';

const ErrorPage404: React.FC = () => {
    return (
        <div className='errorPage404'>
            <h1>This is error 404 component</h1>
        </div>
    );
};

export default ErrorPage404;