import React, { useEffect } from 'react';

// css
import './index.sass';

// three.js
import * as THREE from 'three';
import { CSS3DRenderer, CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer.js';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls.js'; // 軌跡球控制器
import Stats from 'three/examples/jsm/libs/stats.module.js';

let bike_youtube_container: HTMLElement;

let camera: THREE.PerspectiveCamera, scene: THREE.Scene, renderer: CSS3DRenderer;

let controls: TrackballControls, stats: any;

// 初始化
const init = () => {
    bike_youtube_container = document.getElementById('bike_youtube_container') as HTMLElement;
};

// 新增場景
const createScene = () => {
    scene = new THREE.Scene();
};

// 新增相機
const createCamera = () => {
    camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 5000 );
    camera.position.set( 500, 350, 750 );
};

// 新增畫面渲染
const createRenderer = () => {
    renderer = new CSS3DRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.domElement.style.width = '100%';
    renderer.domElement.style.height = '25%';
    // renderer.domElement.style.backgroundColor = '#314CB6';
    bike_youtube_container.appendChild( renderer.domElement );

    window.addEventListener( 'resize', onWindowResize );
};

/**
 * youtube 元素
 * 
 */
class YoutubeElement {
    youtubeId: string;
    x: number;
    y: number;
    z: number;
    ry: number;

    constructor(youtubeId: string, x: number, y: number, z: number, ry: number) {
        this.youtubeId = youtubeId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.ry = ry;
    }

    // 生成 youtube 物件
    generateYoutubeObject = () => {
        const width = '480px';
        const height = '360px';
        const iframe = document.createElement('iframe');
        iframe.style.width = width;
        iframe.style.height = height;
        iframe.style.border = '0px';
        iframe.src = ['https://www.youtube.com/embed/', this.youtubeId, '?rel=0'].join('');

        const youtubeBox = document.createElement('div'); // 新增 youtubeBox
        youtubeBox.style.width = width;
        youtubeBox.style.height = height;
        youtubeBox.appendChild(iframe);

        const object = new CSS3DObject(youtubeBox);
        object.position.set(this.x, this.y, this.z);
        object.rotation.y = this.ry;

        return object;
    };
}

// 創建 youtube 物件
const createYoutube = () => {
    const group = new THREE.Group();
    group.add( new YoutubeElement( 'UmpC9L4k6wM', 0, 0, 240, 0 ).generateYoutubeObject() );
    group.add( new YoutubeElement( 'p8eEJxvqi68', 240, 0, 0, Math.PI / 2 ).generateYoutubeObject() );
    group.add( new YoutubeElement( 'qCQ9Gfyziyo', 0, 0, - 240, Math.PI ).generateYoutubeObject() );
    group.add( new YoutubeElement( 'u3EdNo4cXvM', - 240, 0, 0, - Math.PI / 2 ).generateYoutubeObject() );
    scene.add(group);
};

// 創建狀態器
const createStats = () => {
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.dom.style.zIndex = 5;
    bike_youtube_container.appendChild(stats.dom);
};

// 創建軌跡球控制器
const createTrackControls = () => {
    controls = new TrackballControls(camera, renderer.domElement);
    controls.rotateSpeed = 5;
    controls.panSpeed = 5;
};

// window resize event
const onWindowResize = () => {
    camera.aspect = window.innerWidth / window.innerHeight; //使用者可見整個場景物件改變大小
    camera.updateProjectionMatrix(); // 更新投影矩陣
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.domElement.style.width = '100%';
    renderer.domElement.style.height = '25%';
};

const animate = () => {
    requestAnimationFrame( animate );
    
    controls.update();
    
    stats.update();
    
    renderer.render(scene, camera);

};

const BikeYoutube: React.FC = () => {
    useEffect(() => {
        if (bike_youtube_container === undefined) {
            init(); // 初始話
            createScene(); // 新增場景
            createCamera(); // 新增相機
            createRenderer(); // 渲染畫面
            createYoutube(); // 創建 youtube
            createStats(); // 創建狀態器
            createTrackControls(); // 創建軌跡球控制器
            animate(); // 動態
        }
    }, []);

    return (
        <div id='bike_youtube_container'></div>
    );
};

export default BikeYoutube;