import React from 'react';

// css
import './index.sass';

// components
import AppMain from './components/AppMain';

const Layout: React.FC = () => {
    return (
        <div className='appWapper'>
            <AppMain></AppMain>
        </div>
    );
};

export default Layout;