import React, { Fragment, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const Permission: React.FC = () => {
    const whiteList = ['/404'];
    const navigate = useNavigate();
    const location = useLocation();
    const [token] = useState(true);

    const handlePermission = () => {
        if (token) {
            navigate(location.pathname, {
                replace: true
            });
        }
        else {
            if (whiteList.indexOf(location.pathname) !== -1) {
                navigate(location.pathname, {
                    replace: true
                });
            }
            else {
                navigate('/404', {
                    replace: true
                });
            }
        }
    };

    useEffect(() => {
        handlePermission();
    }, [location.pathname, token]);

    return <Fragment></Fragment>;
};

export default Permission;
