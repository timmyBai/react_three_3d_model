import defaultSettings from '@/settings';

const title: string = defaultSettings.title || 'react base framework typescript';

export const getPageTitle = (pageTitle: string): string => {
    if (pageTitle) {
        return `${pageTitle} - ${title}`;
    }

    return pageTitle;
};
