import React from 'react';
import { BrowserRouter, Navigate } from 'react-router-dom';
import RouterWaiter, { OnRouteBeforeType } from './components/RouterWaiter';

// utils
import { getPageTitle } from '@/utils/getPageTitle';

// components
import Layout from '@/layout/';
import BikeYoutube from '@/views/bikeYoutube';
import ErrorPage404 from '@/views/errorPage/errorPage404';

type RoutersProps = {
    children: JSX.Element | JSX.Element[]
}

// base routes
export const constantRoutes = [
    {
        path: '',
        element: <Layout />,
        children: [
            {
                path: '',
                index: true,
                element: <BikeYoutube />,
                meta: {
                    title: '首頁'
                }
            }
        ]
    },
    {
        path: '/404',
        element: <ErrorPage404 />,
        meta: {
            title: '404'
        },
        hidden: true
    },
    {
        path: '*',
        element: <Navigate to='/404' />,
        hidden: true
    }
];

// permission routes
export const asyncRoutes = [
];

// routes component
const Router: React.FC<RoutersProps> = ({ children }) => {
    const onRouteBefore: OnRouteBeforeType = ({ meta }) => {
        document.title = getPageTitle(meta.title);
    };

    return (
        <BrowserRouter>
            <RouterWaiter routes={constantRoutes} onRouteBefore={onRouteBefore}></RouterWaiter>
            {children}
        </BrowserRouter>
    );
};

export default Router;
